/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.interfaceproject;

/**
 *
 * @author Administrator
 */
public class TestInterfaceAble {
    public static void main(String[] args) {
        Bat bat = new Bat("Bas"); // Animal, Poultry, Flyable
        bat.fly(); 
        
        Plane plane = new Plane("Engine number I"); // Vehicle, Flyable, Runable
        plane.startEngine();
        plane.run();
        plane.fly(); 
        
        Dog dog = new Dog ("Dang"); // Animal, LandAnimal, Runable
        dog.run(); 
        
        Human human = new Human("Hun"); // Animal, LandAnimal, Crawlable, Runable, Swimable
        human.crawl();
        human.run();
        human.swim();
        
        Crocodile crocodile = new Crocodile("Cake"); // Animal, Reptile, Crawlable, Runable, Swimable
        crocodile.crawl();
        crocodile.swim();
        crocodile.run();
        
        Fish fish = new Fish("Fang"); // Animal, AquaticAnimal, Swimable
        fish.swim();
        
//        Flyable[] flyables = {bat, plane};
//        for (Flyable f : flyables) {
//            if (f instanceof Plane) {
//                Plane p = (Plane)f;
//                p.startEngine();
//                p.run();
//            }
//            f.fly();
//        }
    }
}
