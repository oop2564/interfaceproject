/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.interfaceproject;

/**
 *
 * @author Administrator
 */
public class Fish extends AquaticAnimal {
    
    private String nickname;

    public Fish(String nickname) {
        super("Fish");
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println(name + ": " + nickname + " can eat");
    }

    @Override
    public void speak() {
        System.out.println(name + ": " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println(name + ": " + nickname + " can sleep");
    }

    @Override
    public void swim() {
        System.out.println(name + ": " + nickname + " can swim");
    }
}
