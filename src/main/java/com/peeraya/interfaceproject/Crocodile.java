/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.interfaceproject;

/**
 *
 * @author Administrator
 */
public class Crocodile extends Reptile implements Crawlable, Swimable, Runable {
    
    private String nickname;

    public Crocodile(String nickname) {
        super("Crocodile", 4);
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println(name + ": " + nickname + " can eat");
    }

    @Override
    public void speak() {
        System.out.println(name + ": " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println(name + ": " + nickname + " can sleep");
    }

    @Override
    public void crawl() {
        System.out.println(name + ": " + nickname + " can crawl");
    }

    @Override
    public void swim() {
        System.out.println(name + ": " + nickname + " can swim");
    }

    @Override
    public void run() {
        System.out.println(name + ": " + nickname + " can run");
    }
    
}