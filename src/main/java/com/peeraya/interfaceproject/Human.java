/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.interfaceproject;

/**
 *
 * @author Administrator
 */
public class Human extends LandAnimal implements Runable, Swimable, Crawlable {
    private String nickname;

    public Human(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println(name + ": " + nickname + " can run");
    }

    @Override
    public void eat() {
        System.out.println(name + ": " + nickname + " can eat");
    }

    @Override
    public void speak() {
        System.out.println(name + ": " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println(name + ": " + nickname + " can sleep");
    }

    @Override
    public void swim() {
        System.out.println(name + ": " + nickname + " can swim");
    }

    @Override
    public void crawl() {
        System.out.println(name + ": " + nickname + " can crawl");
    }
    
}
